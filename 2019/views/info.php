<p>Chaque année depuis plus de 50 ans, le Rallye d’Hiver vous propose de découvrir un Paris pittoresque à travers le prisme d’une thématique particulière (précédemment la Musique, l’eau, les dames…).</p>
<p>En pratique, des équipes de 2 à 7 personnes ont les trois mois d’hiver pour résoudre une vingtaine d’énigmes qui les conduisent sur des lieux en rapport avec le thème du Rallye. Là, les équipes doivent répondre à un questionnaire qui guide leur découverte du lieu. C’est l’occasion de se cultiver en s’amusant, et d’occuper les longues soirées d’hiver et les weekend brumeux !</p>
<p>Cette année, le Rallye a débuté le 21 décembre 2018 et se terminera le 21 mars 2019. </p>
<div class="info-btn">
	<a href="https://inscriptions.rallyehiver.fr"><button class="btn font2">S'inscrire</button></a><br>
	<a href="https://inscriptions.rallyehiver.fr"><button class="btn font2">Plus d'information</button></a><br>
	<button class="btn font2 mail-orga">Contacter les organisateurs</button>
</div>