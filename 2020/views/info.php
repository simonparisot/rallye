<p>Chaque année depuis plus de 50 ans, le Rallye d’Hiver vous propose de découvrir un Paris pittoresque à travers le prisme d’une thématique particulière (précédemment la musique, les jeux, le luxe …).</p>
<p>En pratique, des équipes de quelques personnes ont les trois mois d’hiver pour résoudre des énigmes qui les conduisent sur des lieux en rapport avec le thème du Rallye. Là, les équipes doivent répondre à un questionnaire qui guide leur découverte du lieu. C’est l’occasion de se cultiver en s’amusant, et d’occuper les longues soirées d’hiver et les weekends brumeux !</p>
<p>Cette année, le Rallye a débuté le 22 décembre 2019 et se terminera le 22 mars 2020. </p>
<div class="info-btn">
	<a href="https://inscriptions.rallyehiver.fr"><button class="btn">S'inscrire</button></a><br>
	<a href="https://inscriptions.rallyehiver.fr"><button class="btn">Plus d'information</button></a><br>
	<button class="btn mail-orga">Contacter les organisateurs</button>
</div>